const presets = [
    ["@babel/preset-env", {
        targets: {
            edge: "17",
            firefox: "60",
            chrome: "67",
            safari: "11.1",
            node: "current"
        },
        useBuiltIns: "usage"
    }],
    "@babel/preset-react",
];

const plugins = [
    "@babel/plugin-proposal-object-rest-spread",
    ["@babel/plugin-proposal-class-properties", { "loose": true }],
    "@babel/plugin-syntax-dynamic-import",
];


module.exports = { presets, plugins };
