module.exports = {
    collectCoverage: true,
    collectCoverageFrom: [
        "**/*.{js,jsx}",
        "!**/*index*",
        "!**/src/test/**"
    ],
    coverageDirectory: "<rootDir>src/test/coverage/",
    moduleFileExtensions: ["js", "jsx"],
    roots: ["src"],
    setupTestFrameworkScriptFile: "<rootDir>src/test/setupTests.js",
    testMatch: ["**/?(*.)+(spec|test).js?(x)"],
    verbose: true,
};
