import path from "path";


const APP_ROOT = path.resolve(__dirname, "./../");
const SRC_DIR = `${APP_ROOT}/src`;
const BUILD_DIR = `${APP_ROOT}/build`;
const CSS_BUNDLE_NAME = (process.env.NODE_ENV === "production") ? "bundle.[chunkhash:8].css" : "bundle.css";
const JS_BUNDLE_NAME = (process.env.NODE_ENV === "production") ? "bundle.[chunkhash:8].js" : "bundle.js";


export default {
    // Pointer to the root source code folder
    context: SRC_DIR,

    devServer: {
        host: "0.0.0.0",
        port: 8080,
        compress: true,
        contentBase: '/www',
    },

   // Main Entrypoint for the application.
   entry: {
        app: "index.entry.jsx",
    },

    // Output location for the final transpiled output
    output: {
        path: BUILD_DIR,
        pathinfo: true,
        publicPath: "/",
        filename: JS_BUNDLE_NAME,
    },

    // Resolvers for folders within the project structure
    resolve: {
        modules: [
            path.resolve(SRC_DIR),
            "node_modules",
        ],
        extensions: [".js", ".jsx"],
    },

    // Build rules for when a file type is encountered during a webpack build
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: "babel-loader",
                exclude: ['node_modules'],
                include: SRC_DIR,
                options: {
                    cacheDirectory: true, // Cache the transformed output, so that you don't have to rebuild everything, when only 1 file changes.
                },
            },
            {
                test: /\.css$/,
                loader: "style-loader",
            }
        ]
    }
}
