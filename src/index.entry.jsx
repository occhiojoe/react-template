import React from "react"; // eslint-disable-line
import { render } from "react-dom";

import App from "application/App";


render(<App />, document.getElementById("app")); // Render the application entrypoint
