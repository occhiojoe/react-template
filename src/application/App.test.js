import React from 'react';

import renderer from "react-test-renderer";
import { shallow } from 'enzyme';

import { TestExample } from 'components/TestExample';

import App from "./App";


describe("App", () => {
    it("Component matches its snapshot...", () => {
        const tree = renderer.create(<App />).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it("Should have at least and at most one TestExample Component...", () => {
        const shallowWrapper = shallow(<App />);
        const test = shallowWrapper.find("TestExample");
        expect(test).toBeDefined();
    });
});
