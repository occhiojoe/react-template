import React, {Component} from "react"; // eslint-disable-line no-unused-vars

import { TestExample } from "components/TestExample";


export default class App extends Component {
    render () {
        return (
            <>
                <p>React Template</p>
                <TestExample />
            </>
        );
    }
}
