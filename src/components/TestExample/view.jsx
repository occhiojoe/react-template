import React, { Component } from "react"; // eslint-disable-line
import PropTypes from "prop-types";


class TestExampleInterface extends Component {
    static propTypes = {
        componentId: PropTypes.string,
        onChange: PropTypes.func,
        onClick: PropTypes.func,
        text: PropTypes.string,
    }

    static defaultProps = {
        componentId: "default",
        onChange: () => {},
        onClick: () => {},
        text: "Hello Universe!"
    }

    static defaultState = {
        inputValue: "default value",
        buttonCount: 0,
    };
}


class TestExample extends TestExampleInterface {
    constructor(props) {
        super(props);
        this.state = TestExampleInterface.defaultState;
    }

    incrementCounter = () => {
        this.setState({ buttonCount: this.state.buttonCount + 1 });
    }

    render() {
        return (
            <div className="test-checkbox">
                <p>
                    {this.props.text}
                </p>
                <input
                    checked="checked"
                    id={this.props.componentId}
                    onClick={this.props.onClick}
                    onChange={this.props.onChange}
                    type="checkbox"
                    value={this.state.inputValue}
                />
                <button onClick={this.incrementCounter}>{this.state.buttonCount}</button>
            </div>
        );
    }
}


export default TestExample;
