import React from 'react';

import renderer from "react-test-renderer";
import { shallow } from 'enzyme';

import TestExample from './view';


describe("Test Component", () => {
    it("Component matches its snapshot...", () => {
        const tree = renderer.create(<TestExample />).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it("Starts with a count of 0...", () => {
        const wrapper = shallow(<TestExample />);
        const buttonText = wrapper.find("button").text();
        expect(buttonText).toEqual("0");
    });

    it("Increments count when button is clicked...", () => {
        const wrapper = shallow(<TestExample />);
        const incrementButton = wrapper.find("button");
        incrementButton.simulate("click");
        const buttonText = wrapper.find("button").text();
        expect(buttonText).toEqual("1");
    });

    it("Has onClick and OnChange methods...", () => {
        const wrapper = shallow(<TestExample />);
        wrapper.instance().props.onClick();
        wrapper.instance().props.onChange();
    });
})
